export default {
   state: {
      posts: []
   },
   actions: {
      async fetchPosts(ctx, limit = 3) { //первый параметр всегда контекст или объект типа {commit, dispatch, getters}, второй - те данные которые мы передаём
         const response = await fetch('https://jsonplaceholder.typicode.com/posts?_limit=' + limit)
         const posts = await response.json()
         ctx.commit('updatePosts', posts)
      }
   },
   mutations: {
      updatePosts(state, posts) { //первый параметр всегда state, потом данные
         state.posts = posts
      },
      createPost(state, newPost){
         state.posts.unshift(newPost)
      }
   },
   getters: {
      allPosts(state) {
         return state.posts
      },
      validPosts(state){
         return state.posts.filter(p => {
            return p.title && p.body
         })
      },
      postsCount(state, getters) {
         return getters.validPosts.length
      },
   }
}